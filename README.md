# README #



### What is this repository for? ###

* Easy example to show Git workflow

### How do I get set up? ###

* Create repo on bitbucket
* Clone repo locally
* Use VSCode to create main.py
* Verify running code
* git status
* git add .
* git commit -m 'Initial Commit'
* git push origin master

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact